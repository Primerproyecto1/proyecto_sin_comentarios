#!/usr/bin/env python
# -- coding: utf-8 --
import random
import time
from subprocess import call
lista = []
lista_final = []
inicio = 0
vecino_vivo = 0

print("***BIENVENIDOS AL JUEGO DE LA VIDA DE CONWAY***")
tamano = int(input("Ingrese la dimensión que tendrá la matriz: "))


def condiciones(i, inicio, tamano, lista, vecino_vivo):
    while (i < (tamano * tamano)):
        if (i == 0) or (i == inicio):
            inicio = inicio + tamano
            if (lista[i] == "[X]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
            if(lista[i] == "[-]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        elif (((i + 1) % tamano) == 0):
            if (lista[i] == "[X]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
            if(lista[i] == "[-]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        else:
            if (lista[i] == "[X]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
            if (lista[i] == "[-]"):
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        reglas(i, vecino_vivo, lista, lista_final)
        i = i + 1
        vecino_vivo = 0
    return lista_final


def reglas(i, vecino_vivo, lista, lista_final):
    if((lista[i] == "[X]") and ((vecino_vivo == 2) or (vecino_vivo == 3))):
        lista_final.append("[X]")
    elif((lista[i] == "[X]") and (vecino_vivo >= 4)):
        lista_final.append("[-]")
    elif((lista[i] == "[X]") and (vecino_vivo <= 1)):
        lista_final.append("[-]")
    elif((lista[i] == "[-]") and (vecino_vivo == 3)):
        lista_final.append("[X]")
    else:
        lista_final.append("[-]")
    return lista_final


def continuidad(tamano, validacion_juego, lista_final):
    stop = 1
    validacion_juego = 0
    for i in range(tamano * tamano):
        if(lista_final[i] == "[-]"):
            validacion_juego = validacion_juego + 1
    if(validacion_juego == (tamano * tamano)):
        stop = 0
    celulas_muertas = validacion_juego
    celulas_vivas = (tamano * tamano) - validacion_juego
    print("Celulas vivas:", celulas_vivas)
    print("Celulas muertas:", celulas_muertas)
    return stop


def imprimir(tamano, lista_final):
    for j in range(tamano * tamano):
        print(lista_final[j], end="")
        if ((j + 1) % tamano == 0):
            print("\n")


for i in range(tamano * tamano):
    estado = random.randint(0, 1)
    if (estado == 0):
        lista.append("[X]")
    else:
        lista.append("[-]")
    print(lista[i], end="")
    if ((i + 1) % tamano == 0):
        print("\n")
stop = 1
while (stop != 0):
    validacion_juego = 1
    i = 0
    condiciones(i, inicio, tamano, lista, vecino_vivo)
    time.sleep(2.4)
    call('clear')
    imprimir(tamano, lista_final)
    stop = continuidad(tamano, validacion_juego, lista_final)
    i = 0
    for i in range(tamano * tamano):
        lista[i] = lista_final[i]
    lista_final = []
